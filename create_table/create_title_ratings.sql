DROP TABLE IF EXISTS title_ratings;
create table title_ratings (
	tconst text,
    averageRating double,
    numVotes int
);