DROP TABLE IF EXISTS title_basics;
create table title_basics (
	tconst text,
    titleType text,
    primaryTitle text,
    originalTitle text,
    isAdult boolean,
    startYear year,
    endYear year,
    runtimeMinutes int,
    genres text
);