DROP TABLE IF EXISTS name_basics;
create table name_basics (
	nconst text,
    primaryName text,
    birthYear text,
    deathYear text,
    primaryProfession text,
    knownForTitles text
);