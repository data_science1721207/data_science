DROP TABLE IF EXISTS title_episode;
create table title_episode (
	tconst text,
    parentTconst text,
    seasonNumber int,
    episodeNumber int
);