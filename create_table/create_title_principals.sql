DROP TABLE IF EXISTS title_principals;
create table title_principals (
	tconst text,
    ordering int,
    nconst text,
    category text,
    job text,
    characters text
);