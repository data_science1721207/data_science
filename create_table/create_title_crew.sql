DROP TABLE IF EXISTS title_crew;
create table title_crew (
	tconst text,
    directors text,
    writers text
);